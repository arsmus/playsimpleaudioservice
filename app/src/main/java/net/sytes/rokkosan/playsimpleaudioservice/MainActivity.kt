package net.sytes.rokkosan.playsimpleaudioservice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getExternalAudioUri()
    }

    fun getExternalAudioUri() {
        Log.d("MyApp", "Tapped to getExternalImageFiles1.")

        // アプリに権限が付与されているかどうかを確認する
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {
                Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is not granted.")
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_READ_EX_STR)
            } else {
                Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is granted.")
                readExMediaAudio()
            }
        } else {
            Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is granted.")
            readExMediaAudio()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permission: Array<String>,
        grantResults: IntArray) {

        Log.d("MyApp", "onRequestPermissionsResult.")

        if (grantResults.size <= 0) {
            return
        }
        when (requestCode) {
            PERMISSION_READ_EX_STR -> {
                run {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // 許可が取れた場合
                        readExMediaAudio()
                    } else {
                        // 許可が取れなかった場合
                        Toast.makeText(
                            this,
                            "パーミッションを取得できません", Toast.LENGTH_LONG
                        ).show()
                    }
                }
                return
            }
        }
    }


    fun readExMediaAudio() {
        var contentUri: Uri
        val columns = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DISPLAY_NAME,
        )

        Log.d("MyApp","URI: " + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)

        val resolver = applicationContext.contentResolver
        val cursor = resolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,  //データの種類
            columns, //取得する項目 nullは全部
            null, //フィルター条件 nullはフィルタリング無し
            null, //フィルター用のパラメータ
            null   //並べ替え
        )
        Log.d( "MyApp" , Arrays.toString( cursor?.getColumnNames() ) )  //項目名の一覧を出力
        val numCount: Int? = cursor?.count
        Log.d("MyApp","Num raws : " + numCount)
        if ( numCount!! < 1 ) {
            Log.d("MyApp","No Media")
            return
        }
        val MediaNum = getRandomCursorNum(numCount)
        Log.d("MyApp","MediaNumRandom : " + MediaNum)

        cursor?.use {
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            val displayNameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            cursor.moveToPosition(MediaNum)
            val id = cursor.getLong(idColumn)
            val displayName = cursor.getString(displayNameColumn)
            contentUri = Uri.withAppendedPath(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                id.toString()
            )
            Log.d(
                "MyApp", "id: $id, name: $displayName, uri: $contentUri"
            )
            Toast.makeText(this, "Play: ${displayName}", Toast.LENGTH_LONG).show()
            playAudio(contentUri)
        }
    }

    fun getRandomCursorNum(maxNum: Int): Int {
        val random = Random()
        return random.nextInt(maxNum)
    }


}